# Start with a base image containing Java runtime https://hub.docker.com/r/adoptopenjdk/openjdk11
FROM gradle:6.8.3-jdk15 as builder
#Possibility to set JVM options (https://www.oracle.com/technetwork/java/javase/tech/vmoptions-jsp-140102.html)
#Set app home folder
ENV APP_HOME=/home/root/build/
WORKDIR $APP_HOME
COPY . .
RUN gradle clean build

# Start with a base image containing Java runtime
FROM openjdk:15-slim as runtime
ENV JAVA_OPTS="-Dspring.profiles.active=dev"
WORKDIR /home/root/user/
COPY --from=builder /home/root/build/build/libs/xam_nhap_man-0.1.0.jar $WORKDIR
EXPOSE 8080
ENTRYPOINT [ "sh", "-c", "java -jar $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom xam_nhap_man-0.1.0.jar" ]