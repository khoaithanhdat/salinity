package vn.com.cmcts.salinity.endpoint;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.com.cmcts.salinity.model.payload.request.StationRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.StationRequestUpdate;
import vn.com.cmcts.salinity.model.payload.response.Result;
import vn.com.cmcts.salinity.model.payload.response.StationResponse;
import vn.com.cmcts.salinity.model.payload.response.StationResponsePagination;
import vn.com.cmcts.salinity.services.StationService;


import java.util.List;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 16:11
 * @updated_by dat
 * @updated_date 06/01/2022 16:11
 * @since 06/01/2022 16:11
 */

@CrossOrigin(
        origins = {"*"},
        allowedHeaders = {"*"}
)
@RestController
@RequestMapping(value = "/api/v1/station")
@Api(value = "Station Management Service", description = "Station Management Service")
public class StationController {

    @Autowired
    private StationService stationService;

    @PostMapping(value = "")
    @ApiOperation(
            value = "Create station",
            notes = "Create station",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<StationResponse>> createStation(@RequestBody StationRequestCreate model) {
        Result<StationResponse> stationResponse = null;
        try {
            stationResponse = stationService.create(model);

        } catch (Exception e) {
            Result<StationResponse> error = Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
            return ResponseEntity.ok(error);
        }

        return ResponseEntity.ok().body(stationResponse);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(
            value = "Update station",
            notes = "Update station",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<StationResponse>> updateStation(@RequestBody StationRequestUpdate model,
                                                                 @PathVariable String id) {
        Result<StationResponse> stationResponse = null;
        try {
            stationResponse = stationService.update(model, id);

        } catch (Exception e) {
            Result<StationResponse> error = Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
            return ResponseEntity.ok(error);
        }

        return ResponseEntity.ok().body(stationResponse);
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(
            value = "Delete station",
            notes = "Delete station",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = String.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<StationResponse>> deleteStation(@PathVariable String id) {
        Result<StationResponse> stationResponse = null;
        try {
            stationResponse = stationService.delete(id);
        } catch (Exception e) {
            Result<StationResponse> error = Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
            return ResponseEntity.ok(error);
        }
        return ResponseEntity.ok().body(stationResponse);
    }


    @GetMapping(value = "/all")
    @ApiOperation(
            value = "Get all station.",
            notes = "Get all station.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<StationResponsePagination>> getAll(@RequestParam(defaultValue = "0", required = false) Integer page,
                                                                    @RequestParam(defaultValue = "0", required = false) Integer size) {
        Pageable pageable = null;
        if (page == 0 && size == 0) {
            pageable = PageRequest.of(0, Integer.MAX_VALUE);
        } else {
            pageable = PageRequest.of(Math.max(0, page - 1), size);
        }
        Result<StationResponsePagination> stationResponsePagination = stationService.getAll(pageable);

        return ResponseEntity.ok().body(stationResponsePagination);
    }
}
