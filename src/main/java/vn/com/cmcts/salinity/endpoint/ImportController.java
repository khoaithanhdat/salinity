package vn.com.cmcts.salinity.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.service.ResponseMessage;
import vn.com.cmcts.salinity.model.payload.request.ImportRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.ImportRequestUpdate;
import vn.com.cmcts.salinity.model.payload.request.OperationAuditRequestSearch;
import vn.com.cmcts.salinity.model.payload.response.ImportResponse;
import vn.com.cmcts.salinity.model.payload.response.ImportResponsePage;
import vn.com.cmcts.salinity.model.payload.response.OperationAuditResponsePagination;
import vn.com.cmcts.salinity.model.payload.response.Result;
import vn.com.cmcts.salinity.services.ImportService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@CrossOrigin(
        origins = {"*"},
        allowedHeaders = {"*"}
)
@RestController
@RequestMapping(value = "/api/v1/import")
@Api(value = "Import Service", description = "Import Service")
public class ImportController {

    @Autowired
    private ImportService importService;

    @PostMapping(value = "/upfile",
            consumes = {
                    MediaType.MULTIPART_FORM_DATA_VALUE,
                    MediaType.APPLICATION_JSON_VALUE
            })
    @ApiOperation(
            value = "Import data.",
            notes = "Import data.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<String>> getFile(@RequestParam("file") @Valid @NotNull @NotBlank MultipartFile file,
                                                   @RequestParam("option") @Valid String ImportRequestUploadFileString) throws Exception {

        return importService.importFile(file, ImportRequestUploadFileString);
    }

    @GetMapping(value = "/list")
    @ApiOperation(
            value = "Get import data by condition.",
            notes = "Get import data by condition.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<ImportResponsePage>> getByCondition(@RequestParam(defaultValue = "1", required = false) Integer page,
                                                                     @RequestParam(defaultValue = "5", required = false) Integer size) {
        Result<ImportResponsePage> importResponsePageResult = importService
                .getByCondition(PageRequest.of(Math.max(0, page - 1), size));

        return ResponseEntity.ok().body(importResponsePageResult);
    }

    @PostMapping(value = "")
    @ApiOperation(
            value = "Create import data.",
            notes = "Create import data.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<ImportResponse>> create(@RequestBody ImportRequestCreate importRequestCreate) throws Exception {
        Result<ImportResponse> importResponseResult = importService.create(importRequestCreate);

        return ResponseEntity.ok().body(importResponseResult);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(
            value = "Update import data by id.",
            notes = "Update import data by id.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<ImportResponse>> updateById(@PathVariable String id, @RequestBody ImportRequestUpdate importRequestUpdate) throws Exception {
        Result<ImportResponse> importResponseResult = importService.updateById(id, importRequestUpdate);

        return ResponseEntity.ok().body(importResponseResult);
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(
            value = "Delete import data by id.",
            notes = "Delete import data by id.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<ImportResponse>> deleteById(@PathVariable String id) throws Exception {
        Result<ImportResponse> importResponseResult = importService.deleteById(id);

        return ResponseEntity.ok().body(importResponseResult);
    }



}
