package vn.com.cmcts.salinity.endpoint;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.com.cmcts.salinity.model.payload.request.SettingRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.SettingRequestUpdate;
import vn.com.cmcts.salinity.model.payload.response.*;
import vn.com.cmcts.salinity.services.SettingService;

import java.util.List;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 17:27
 * @updated_by dat
 * @updated_date 06/01/2022 17:27
 * @since 06/01/2022 17:27
 */

@CrossOrigin(
        origins = {"*"},
        allowedHeaders = {"*"}
)
@RestController
@RequestMapping(value = "/api/v1/setting")
@Api(value = "Setting Management Service", description = "Setting Management Service")
public class SettingController {

    @Autowired
    private SettingService settingService;


    @PostMapping(value = "")
    @ApiOperation(
            value = "Create or Update setting",
            notes = "Create or Update setting",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<SettingResponse>> updateSetting(@RequestBody SettingRequestUpdate model) {
        Result<SettingResponse> settingResponse = null;
        try {
            settingResponse = settingService.update(model);

        } catch (Exception e) {
            Result<SettingResponse> error = Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
            return ResponseEntity.ok(error);
        }

        return ResponseEntity.ok().body(settingResponse);
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(
            value = "Delete setting",
            notes = "Delete setting",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = String.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<SettingResponse>> deleteStation(@PathVariable String id) {
        Result<SettingResponse> settingResponse = null;
        try {
            settingResponse = settingService.delete(id);
        } catch (Exception e) {
            Result<SettingResponse> error = Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
            return ResponseEntity.ok(error);
        }
        return ResponseEntity.ok().body(settingResponse);
    }


    @GetMapping(value = "/all")
    @ApiOperation(
            value = "Get all setting.",
            notes = "Get all setting.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<SettingResponsePagination>> getAll(@RequestParam(defaultValue = "1", required = false) Integer page,
                                                                    @RequestParam(defaultValue = "5", required = false) Integer size) {
        Result<SettingResponsePagination> settingResponsePagination = settingService.getAll(PageRequest.of(Math.max(0, page - 1), size));

        return ResponseEntity.ok().body(settingResponsePagination);
    }

    @GetMapping(value = "/{name}")
    @ApiOperation(
            value = "Get value by name.",
            notes = "Get value by name.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<String>> getValueByName(@PathVariable String name) {
        Result<String> value = settingService.getByName(name);

        return ResponseEntity.ok().body(value);
    }
}
