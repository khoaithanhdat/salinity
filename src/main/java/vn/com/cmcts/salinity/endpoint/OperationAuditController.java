package vn.com.cmcts.salinity.endpoint;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.com.cmcts.salinity.model.payload.request.OperationAuditRequest;
import vn.com.cmcts.salinity.model.payload.request.OperationAuditRequestSearch;
import vn.com.cmcts.salinity.model.payload.response.*;
import vn.com.cmcts.salinity.services.OperationAuditService;

import java.util.List;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 19:56
 * @updated_by dat
 * @updated_date 06/01/2022 19:56
 * @since 06/01/2022 19:56
 */

@CrossOrigin(
        origins = {"*"},
        allowedHeaders = {"*"}
)
@RestController
@RequestMapping(value = "/api/v1/operation-audit")
@Api(value = "Operation Audit Management Service", description = "Operation Audit Management Service")
public class OperationAuditController {

    @Autowired
    private OperationAuditService operationAuditService;

    @PostMapping(value = "")
    @ApiOperation(
            value = "Create operation audit",
            notes = "Create operation audit",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<OperationAuditResponse>> createOperationAudit(@RequestBody OperationAuditRequest model) {
        Result<OperationAuditResponse> operationAuditResponse = null;
        try {
            operationAuditResponse = operationAuditService.create(model);

        } catch (Exception e) {
            Result<OperationAuditResponse> error = Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
            return ResponseEntity.ok(error);
        }

        return ResponseEntity.ok().body(operationAuditResponse);
    }

    @PostMapping(value = "/list")
    @ApiOperation(
            value = "Get operation audit by condition.",
            notes = "Get operation audit by condition.",
            response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", reference = "The server successfully processed the request.", response = List.class),
            @ApiResponse(code = 400, message = "Bad Request", reference = "Requested action could not be understood by the system.", response = ResponseEntity.class),
            @ApiResponse(code = 401, message = "Unauthorized", reference = "Requested action requires authentication.", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Forbidden", reference = "System refuses to fulfill the requested action.", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal Server Error", reference = "A generic error has occurred on the system.", response = ResponseEntity.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "token", paramType = "header", dataType = "string", required = true)
    })
    public ResponseEntity<Result<OperationAuditResponsePagination>> getByCondition(@RequestBody OperationAuditRequestSearch operationAuditRequestSearch,
                                                                                   @RequestParam(defaultValue = "1", required = false) Integer page,
                                                                                   @RequestParam(defaultValue = "5", required = false) Integer size) {
        Result<OperationAuditResponsePagination> operationAuditResponsePagination = operationAuditService
                .getByCondition(operationAuditRequestSearch, PageRequest.of(Math.max(0, page - 1), size));

        return ResponseEntity.ok().body(operationAuditResponsePagination);
    }
}
