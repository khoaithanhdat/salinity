package vn.com.cmcts.salinity.enumeration;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 18:05
 * @updated_by dat
 * @updated_date 06/01/2022 18:05
 * @since 06/01/2022 18:05
 */
public enum DateTimeEnum {

    yyyy_MM_dd("yyyy-MM-dd", "yyyy-MM-dd"),
    yyyy_MM_dd_HHmmss("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"),
    yyyyMMdd_T_HHmmssSSSXXX("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
    yyyyMMdd_T_HHmmssSSSZ("yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
    ddMMyyyHHmmss("dd-MM-yyyy HH:mm:ss", "dd-MM-yyyy HH:mm:ss"),
    dd_MM_yyyy("dd-MM-yyyy", "dd-MM-yyyy"),
    ddMMyyyy("ddMMyyyy", "ddMMyyyy"),
    yyyyMMdd("yyyyMMdd", "yyyyMMdd"),
    dd_MM_yyy_HHmm("dd-MM-yyyy HH:mm","dd-MM-yyyy HH:mm");

    private final String value;
    private final String displayName;

    public String getValue() {
        return this.value;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    private DateTimeEnum(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }
}
