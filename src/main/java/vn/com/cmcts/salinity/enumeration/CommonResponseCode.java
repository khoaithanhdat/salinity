package vn.com.cmcts.salinity.enumeration;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 17:10
 * @updated_by dat
 * @updated_date 06/01/2022 17:10
 * @since 06/01/2022 17:10
 */
public enum CommonResponseCode {
    SUCCESS(200, "Thành công"),
    CREATED(201, "Tạo mới thành công"),
    BAD_REQUEST(400, "Request không hợp lệ"),
    UNAUTHORIZE(401, "Không có quyền truy cập"),
    FORBIDDEN(403, "Không đủ quyền truy cập"),
    NOT_FOUND(404, "Không tìm thấy data"),
    INTERNAL_ERROR(500, "Máy chủ đang bận"),
    BAD_GATEWAY(502, "Bad gateway");

    private final int value;
    private final String displayName;

    public Integer getValue() {
        return this.value;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    private CommonResponseCode(int value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }
}
