package vn.com.cmcts.salinity.model.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 16:35
 * @updated_by dat
 * @updated_date 06/01/2022 16:35
 * @since 06/01/2022 16:35
 */

@Data
public class SettingRequestBase implements Serializable {

    @JsonProperty("name")
    private String name;

    @JsonProperty("value")
    private String value;
}
