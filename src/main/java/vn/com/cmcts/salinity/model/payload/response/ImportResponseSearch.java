package vn.com.cmcts.salinity.model.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ImportResponseSearch extends ImportResponse {
    @JsonProperty("station_name")
    private String station_name;

    @JsonProperty("station_type")
    private String station_type;

    public ImportResponseSearch() {
    }

    public ImportResponseSearch(String id,
                                long no,
                                Date day,
                                int time,
                                Double salinity,
                                String station,
                                String station_name,
                                String station_type) {
        this.setId(id);
        this.setNo(no);
        this.setDay(day);
        this.setTime(time);
        this.setSalinity(salinity);
        this.setStation(station);
        this.setStation_name(station_name);
        this.setStation_type(station_type);
    }
}
