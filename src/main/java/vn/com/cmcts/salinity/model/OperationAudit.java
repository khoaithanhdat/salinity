package vn.com.cmcts.salinity.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import vn.com.cmcts.common.model.AuditableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 16:07
 * @updated_by dat
 * @updated_date 06/01/2022 16:07
 * @since 06/01/2022 16:07
 */
@Entity
@Table(name = "operation_audit")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OperationAudit extends AuditableEntity {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @JsonProperty("id")
    private String id;

    @Column(name = "user_name")
    @JsonProperty("user_name")
    private String userName;

    @Column(name = "operation")
    @JsonProperty("operation")
    private String operation;

    @Column(name = "operation_time")
    @JsonProperty("operation_time")
    private Long operationTime;
}
