package vn.com.cmcts.salinity.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import vn.com.cmcts.common.model.AuditableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 11/01/2022 16:24
 * @updated_by dat
 * @updated_date 11/01/2022 16:24
 * @since 11/01/2022 16:24
 */

@Entity
@Table(name = "roles")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Role extends AuditableEntity {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @JsonProperty("id")
    private String id;

    @Column(name = "role_name")
    @JsonProperty("role_name")
    private String roleName;

    @Column(name = "role_description")
    @JsonProperty("role_description")
    private String roleDescription;

    @Column(name = "role_identification")
    @JsonProperty("role_identification")
    private Integer roleIdentification;
}
