package vn.com.cmcts.salinity.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import vn.com.cmcts.common.model.AuditableEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "import")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Import extends AuditableEntity {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @JsonProperty("id")
    private String id;

    @Column(name = "no")
    @JsonProperty("no")
    private long no;

    @Column(name = "day")
    private Date day;

    @Column(name = "time")
    @JsonProperty("time")
    private int time;

    @Column(name = "salinity")
    @JsonProperty("salinity")
    private Double salinity;

    @Column(name = "station")
    @JsonProperty("station")
    private String station;
}
