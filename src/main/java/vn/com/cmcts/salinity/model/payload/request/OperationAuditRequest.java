package vn.com.cmcts.salinity.model.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 17:44
 * @updated_by dat
 * @updated_date 06/01/2022 17:44
 * @since 06/01/2022 17:44
 */

@Data
public class OperationAuditRequest implements Serializable {

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("operation")
    private String operation;
}
