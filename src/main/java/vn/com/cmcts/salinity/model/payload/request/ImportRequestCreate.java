package vn.com.cmcts.salinity.model.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ImportRequestCreate implements Serializable {
    @JsonProperty("no")
    private long no;

    @JsonProperty("day")
    private Date day;

    @JsonProperty("time")
    private int time;

    @JsonProperty("salinity")
    private Double salinity;

    @JsonProperty("station")
    private String station;
}
