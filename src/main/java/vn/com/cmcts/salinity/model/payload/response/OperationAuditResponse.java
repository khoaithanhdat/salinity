package vn.com.cmcts.salinity.model.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 17:45
 * @updated_by dat
 * @updated_date 06/01/2022 17:45
 * @since 06/01/2022 17:45
 */

@Data
public class OperationAuditResponse implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("operation")
    private String operation;

    @JsonProperty("operation_time")
    private Long operationTime;
}
