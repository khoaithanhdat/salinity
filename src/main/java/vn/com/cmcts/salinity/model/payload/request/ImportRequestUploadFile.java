package vn.com.cmcts.salinity.model.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ImportRequestUploadFile implements Serializable {
    @JsonProperty("year_input")
    private Integer year_input;

    @JsonProperty("line_from")
    private Integer line_from;

    @JsonProperty("line_to")
    private Integer line_to;

    @JsonProperty("overwrite")
    private boolean overwrite;

    @JsonProperty("station")
    private String station;
}
