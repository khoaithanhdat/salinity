package vn.com.cmcts.salinity.model.payload.request;

import lombok.Data;

import java.io.Serializable;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 15:38
 * @updated_by dat
 * @updated_date 06/01/2022 15:38
 * @since 06/01/2022 15:38
 */

@Data
public class StationRequestCreate extends StationRequestBase implements Serializable {
}
