package vn.com.cmcts.salinity.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import vn.com.cmcts.common.model.AuditableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 16:06
 * @updated_by dat
 * @updated_date 06/01/2022 16:06
 * @since 06/01/2022 16:06
 */
@Entity
@Table(name = "station")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Station extends AuditableEntity {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @JsonProperty("id")
    private String id;

    @Column(name = "name")
    @JsonProperty("name")
    private String name;

    @Column(name = "code")
    @JsonProperty("code")
    private String code;

    @Column(name = "station_type")
    @JsonProperty("station_type")
    private String stationType;

    @Column(name = "address")
    @JsonProperty("address")
    private String address;

    @Column(name = "river")
    @JsonProperty("river")
    private String river;

    @Column(name = "latitude")
    @JsonProperty("latitude")
    private String latitude;

    @Column(name = "longitude")
    @JsonProperty("longitude")
    private String longitude;

    @Column(name = "input")
    @JsonProperty("input")
    private String input;

    @Column(name = "output")
    @JsonProperty("output")
    private String output;

    @Column(name = "province")
    @JsonProperty("province")
    private String province;
}
