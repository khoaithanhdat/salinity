package vn.com.cmcts.salinity.model.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ImportResponse implements Serializable {
    @JsonProperty("id")
    private String id;

    @JsonProperty("no")
    private long no;

    @JsonProperty("day")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date day;

    @JsonProperty("time")
    private int time;

    @JsonProperty("salinity")
    private Double salinity;

    @JsonProperty("station")
    private String station;
}
