package vn.com.cmcts.salinity.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import vn.com.cmcts.common.model.AuditableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 11/01/2022 16:05
 * @updated_by dat
 * @updated_date 11/01/2022 16:05
 * @since 11/01/2022 16:05
 */

@Entity
@Table(name = "users")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User extends AuditableEntity {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @JsonProperty("id")
    private String id;

    @Column(name = "username")
    @JsonProperty("username")
    private String username;

    @Column(name = "email")
    @JsonProperty("email")
    private String email;

    @Column(name = "password")
    @JsonProperty("password")
    private String password;

    @Column(name = "role_id")
    @JsonProperty("role_id")
    private String roleId;

    @Column(name = "customer_price_code")
    @JsonProperty("customer_price_code")
    private String customerPriceCode;

    @Column(name = "customer_name1")
    @JsonProperty("customer_name1")
    private String customerName1;

    @Column(name = "customer_name2")
    @JsonProperty("customer_name2")
    private String customerName2;

    @Column(name = "postal_code")
    @JsonProperty("postal_code")
    private String postalCode;

    @Column(name = "address1")
    @JsonProperty("address1")
    private String address1;

    @Column(name = "address2")
    @JsonProperty("address2")
    private String address2;

    @Column(name = "address3")
    @JsonProperty("address3")
    private String address3;

    @Column(name = "phone_number")
    @JsonProperty("phone_number")
    private String phoneNumber;

    @Column(name = "fax_number")
    @JsonProperty("fax_number")
    private String faxNumber;

    @Column(name = "representative")
    @JsonProperty("representative")
    private String representative;

    @Column(name = "parent_code")
    @JsonProperty("parent_code")
    private String parentCode;

    @Column(name = "refresh_token")
    @JsonProperty("refresh_token")
    private String refreshToken;

    @Column(name = "group")
    @JsonProperty("group")
    private String group;

    @Column(name = "department")
    @JsonProperty("department")
    private String department;

    @Column(name = "status")
    @JsonProperty("status")
    private Integer roleStatus;


}
