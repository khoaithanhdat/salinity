package vn.com.cmcts.salinity.model.payload.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.cmcts.salinity.enumeration.CommonResponseCode;

import java.io.Serializable;
import java.util.List;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 17:06
 * @updated_by dat
 * @updated_date 06/01/2022 17:06
 * @since 06/01/2022 17:06
 */

@Data
@NoArgsConstructor
public class Result<T> implements Serializable {

    private int code;
    private String message;
    private T data;

    public Result(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static <T> Result<T> ok(int code, String message, T data) {
        return new Result(CommonResponseCode.SUCCESS.getValue(), CommonResponseCode.SUCCESS.getDisplayName(), data);
    }

    public static <T> Result<T> error(int code, String message) {
        return new Result(code, message);
    }

    public Result(CommonResponseCode enums, T data) {
        this.code = enums.getValue();
        this.message = enums.getDisplayName();
        this.data = data;
    }

    public static <T> Result<T> ok(T data) {
        return new Result(CommonResponseCode.SUCCESS, data);
    }
}
