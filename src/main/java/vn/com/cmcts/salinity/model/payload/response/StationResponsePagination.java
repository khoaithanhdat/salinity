package vn.com.cmcts.salinity.model.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 15:58
 * @updated_by dat
 * @updated_date 06/01/2022 15:58
 * @since 06/01/2022 15:58
 */

@Data
public class StationResponsePagination implements Serializable {

    @JsonProperty("stations")
    private List<StationResponse> stationResponses;

    private long total;
}
