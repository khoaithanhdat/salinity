package vn.com.cmcts.salinity.model.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 15:38
 * @updated_by dat
 * @updated_date 06/01/2022 15:38
 * @since 06/01/2022 15:38
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StationResponse implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("code")
    private String code;

    @JsonProperty("station_type")
    private String stationType;

    @JsonProperty("address")
    private String address;

    @JsonProperty("status")
    private String status;

    @JsonProperty("river")
    private String river;

    @JsonProperty("latitude")
    private String latitude;

    @JsonProperty("longitude")
    private String longitude;

    @JsonProperty("input")
    private String input;

    @JsonProperty("output")
    private String output;

    @JsonProperty("province")
    private String province;

}
