package vn.com.cmcts.salinity.model.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ImportResponsePage implements Serializable {

    @JsonProperty("data_import")
    private List<ImportResponseSearch> importResponseList;

    private long total;
}
