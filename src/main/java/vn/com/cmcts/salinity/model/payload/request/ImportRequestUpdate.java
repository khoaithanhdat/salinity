package vn.com.cmcts.salinity.model.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ImportRequestUpdate implements Serializable {

    @JsonProperty("day")
    private Date day;

    @JsonProperty("time")
    private int time;

    @JsonProperty("salinity")
    private Double salinity;

    @JsonProperty("station")
    private String station;
}
