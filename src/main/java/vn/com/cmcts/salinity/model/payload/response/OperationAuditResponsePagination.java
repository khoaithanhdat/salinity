package vn.com.cmcts.salinity.model.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 18:13
 * @updated_by dat
 * @updated_date 06/01/2022 18:13
 * @since 06/01/2022 18:13
 */

@Data
public class OperationAuditResponsePagination implements Serializable {

    @JsonProperty("operation_audit_response")
    private List<OperationAuditResponseSearch> operationAuditResponseSearches;

    private long total;
}
