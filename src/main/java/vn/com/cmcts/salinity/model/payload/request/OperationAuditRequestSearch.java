package vn.com.cmcts.salinity.model.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 18:14
 * @updated_by dat
 * @updated_date 06/01/2022 18:14
 * @since 06/01/2022 18:14
 */

@Data
public class OperationAuditRequestSearch implements Serializable {

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("from_time")
    private Long fromTime;

    @JsonProperty("to_time")
    private Long toTime;
}
