package vn.com.cmcts.salinity.util;

import vn.com.cmcts.salinity.enumeration.DateTimeEnum;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 18:04
 * @updated_by dat
 * @updated_date 06/01/2022 18:04
 * @since 06/01/2022 18:04
 */
public class DateUtils {

    public static Long toDateLong(Long milis, DateTimeEnum format) {
        return toDateLong(milis, format.getValue());
    }

    public static Long toDateLong(Long milis, String format) {
        try {
            if (milis != null || !milis.equals(0L)) {
                SimpleDateFormat formatter = new SimpleDateFormat(format);
                Date date = new Date(milis);

                return Long.valueOf(formatter.format(date));
            } else
                return null;
        } catch (Exception var3) {
            var3.printStackTrace();
            return null;
        }
    }

    public static String convertLongToStringDateFormat(Long dateLong, DateTimeEnum dateTimeEnum) {
        String sDate = String.valueOf(dateLong);
        if (sDate != null) {
            String year = sDate.substring(0, 4);
            String month = sDate.substring(4, 6);
            String day = sDate.substring(6, 8);
            switch (dateTimeEnum) {
                case dd_MM_yyyy -> sDate = day + "-" + month + "-" + year;
                case yyyy_MM_dd -> sDate = year + "-" + month + "-" + day;
                default -> sDate = "";
            }

        }
        return sDate;
    }

    public static String convertLongToStringFormatDateTime(Long miliseconds, DateTimeEnum dateTimeEnum) {
        if (miliseconds != null && !miliseconds.equals(0L)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateTimeEnum.getValue());
            Date date = new Date(miliseconds);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.HOUR_OF_DAY, 7);

            return simpleDateFormat.format(cal.getTime());
        }

        return "";
    }


    public static Long getMinimumDate(Long miliseconds) {
        if (miliseconds != null && !miliseconds.equals(0L)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(miliseconds));
            cal.add(Calendar.HOUR_OF_DAY, 7);
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0, 0);

            Date desiredDate = cal.getTime();
            return desiredDate.getTime();
        }
        return 0L;
    }

    public static Long getMaximumDate(Long miliseconds) {
        if (miliseconds != null && !miliseconds.equals(0L)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(miliseconds));
            cal.add(Calendar.HOUR_OF_DAY, 7);
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 23, 59, 59);

            Date desiredDate = cal.getTime();
            return desiredDate.getTime();
        }
        return 0L;
    }

}
