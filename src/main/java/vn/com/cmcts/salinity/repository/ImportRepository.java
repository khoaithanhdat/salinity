package vn.com.cmcts.salinity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.com.cmcts.salinity.model.Import;
import vn.com.cmcts.salinity.model.payload.response.ImportResponseSearch;

import java.awt.*;
import java.util.List;

public interface ImportRepository extends JpaRepository<Import, String> {

    @Query(value = "select new vn.com.cmcts.salinity.model.payload.response.ImportResponseSearch(a.id, a.no, a.day, a.time, a.salinity, a.station, b.name, b.stationType) " +
            "from Import a " +
            "left join Station b on a.station = b.code " +
            "where 1=1 ")
    Page<ImportResponseSearch> findDataByCondition(Pageable pageable);

    @Query(value = "select count(a.code) from Station a where a.code = :stationCode")
    int checkStationCode(@Param("stationCode") String stationCode);

    @Query(value = "select a from Import a where 1=1 " +
            "and a.station = :station " +
            "and (:year is null or extract(year from a.day) = :year )")
    List<Import> findDataOverWrite(@Param("station") String station, @Param("year") Integer year);
}