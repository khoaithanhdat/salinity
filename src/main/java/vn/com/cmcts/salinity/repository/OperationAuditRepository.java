package vn.com.cmcts.salinity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.com.cmcts.salinity.model.OperationAudit;
import vn.com.cmcts.salinity.model.payload.request.OperationAuditRequestSearch;

import java.util.List;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 17:47
 * @updated_by dat
 * @updated_date 06/01/2022 17:47
 * @since 06/01/2022 17:47
 */

@Repository
public interface OperationAuditRepository extends JpaRepository<OperationAudit, String> {

    @Query(value = "SELECT o FROM OperationAudit o" +
            " WHERE (:#{#operationAuditRequestSearch.userName} is null or :#{#operationAuditRequestSearch.userName} = ''" +
            " or o.userName = :#{#operationAuditRequestSearch.userName}) " +
            " AND (:#{#operationAuditRequestSearch.fromTime} is null or :#{#operationAuditRequestSearch.fromTime} = 0L " +
            " or o.operationTime >= :#{#operationAuditRequestSearch.fromTime}) AND (:#{#operationAuditRequestSearch.toTime} is null " +
            " or :#{#operationAuditRequestSearch.toTime} = 0L or o.operationTime <= :#{#operationAuditRequestSearch.toTime})")
    Page<OperationAudit> findByCondition(OperationAuditRequestSearch operationAuditRequestSearch, Pageable pageable);
}
