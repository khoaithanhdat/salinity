package vn.com.cmcts.salinity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.com.cmcts.salinity.model.Setting;
import vn.com.cmcts.salinity.model.payload.response.SettingResponse;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 16:38
 * @updated_by dat
 * @updated_date 06/01/2022 16:38
 * @since 06/01/2022 16:38
 */

@Repository
public interface SettingRepository extends JpaRepository<Setting,String> {

    @Query("SELECT new vn.com.cmcts.salinity.model.payload.response.SettingResponse(" +
            "id,name,value)" +
            " FROM Setting")
    Page<SettingResponse> getAll(Pageable pageable);

    @Query("SELECT value FROM Setting WHERE name = :name")
    String getValueByName(String name);

    @Query("SELECT id FROM Setting WHERE name = :name")
    String findIdByName(String name);

    Setting findByName(String name);


}
