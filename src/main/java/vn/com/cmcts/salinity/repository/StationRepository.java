package vn.com.cmcts.salinity.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.com.cmcts.salinity.model.Station;
import vn.com.cmcts.salinity.model.payload.response.StationResponse;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 15:39
 * @updated_by dat
 * @updated_date 06/01/2022 15:39
 * @since 06/01/2022 15:39
 */
@Repository
public interface StationRepository extends JpaRepository<Station, String> {

    @Query("SELECT new vn.com.cmcts.salinity.model.payload.response.StationResponse(" +
            "s.id,s.name,s.code,s.stationType,s.address,s.status,s.river,s.latitude,s.longitude,s.input,s.output,s.province)" +
            " FROM Station s")
    Page<StationResponse> getAll(Pageable pageable);
}
