package vn.com.cmcts.salinity.services;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import vn.com.cmcts.salinity.model.payload.request.ImportRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.ImportRequestUpdate;
import vn.com.cmcts.salinity.model.payload.response.ImportResponse;
import vn.com.cmcts.salinity.model.payload.response.ImportResponsePage;
import vn.com.cmcts.salinity.model.payload.response.Result;


public interface ImportService {
    ResponseEntity<Result<String>> importFile(MultipartFile file, String iImportRequestUploadFileString) throws Exception;

    Result<ImportResponsePage> getByCondition(Pageable pageable);

    Result<ImportResponse> create(ImportRequestCreate importRequestCreate) throws Exception;

    Result<ImportResponse> updateById(String id, ImportRequestUpdate importRequestUpdate) throws Exception;

    Result<ImportResponse> deleteById(String id) throws Exception;
}
