package vn.com.cmcts.salinity.services.impl;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.com.cmcts.salinity.enumeration.DateTimeEnum;
import vn.com.cmcts.salinity.model.OperationAudit;
import vn.com.cmcts.salinity.model.payload.request.OperationAuditRequest;
import vn.com.cmcts.salinity.model.payload.request.OperationAuditRequestSearch;
import vn.com.cmcts.salinity.model.payload.response.OperationAuditResponse;
import vn.com.cmcts.salinity.model.payload.response.OperationAuditResponsePagination;
import vn.com.cmcts.salinity.model.payload.response.OperationAuditResponseSearch;
import vn.com.cmcts.salinity.model.payload.response.Result;
import vn.com.cmcts.salinity.repository.OperationAuditRepository;
import vn.com.cmcts.salinity.services.OperationAuditService;
import vn.com.cmcts.salinity.util.DateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 17:48
 * @updated_by dat
 * @updated_date 06/01/2022 17:48
 * @since 06/01/2022 17:48
 */

@Service
public class OperationAuditServiceImpl implements OperationAuditService {

    @Autowired
    private OperationAuditRepository operationAuditRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public Result<OperationAuditResponse> create(OperationAuditRequest operationAuditRequest) throws Exception {
        OperationAudit operationAudit = new OperationAudit();
        BeanUtils.copyProperties(operationAuditRequest, operationAudit);
        operationAudit.setId(UUID.randomUUID().toString());
        operationAudit.setOperationTime(System.currentTimeMillis());

        operationAuditRepository.save(operationAudit);

        OperationAuditResponse operationAuditResponse = Optional.of(operationAudit).map(this::getOperationAuditResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("operation.audit.create.error",null,null)));

        return Result.ok(operationAuditResponse);
    }

    @Override
    public Result<OperationAuditResponsePagination> getByCondition(OperationAuditRequestSearch operationAuditRequestSearch, Pageable pageable) {
        List<OperationAuditResponseSearch> operationAuditResponseSearches = null;
        if(!ObjectUtils.defaultIfNull(operationAuditRequestSearch.getFromTime(), 0L).equals(0L)){
            Long fromTime = DateUtils.getMinimumDate(operationAuditRequestSearch.getFromTime());
            operationAuditRequestSearch.setFromTime(fromTime);
        }
        if(!ObjectUtils.defaultIfNull(operationAuditRequestSearch.getToTime(), 0L).equals(0L)){
            Long toTime = DateUtils.getMaximumDate(operationAuditRequestSearch.getToTime());
            operationAuditRequestSearch.setToTime(toTime);
        }

        Page<OperationAudit> operationAudits = operationAuditRepository.findByCondition(operationAuditRequestSearch, pageable);
        if (!CollectionUtils.isEmpty(operationAudits.getContent())) {
            operationAuditResponseSearches = operationAudits.getContent().stream().map(operationAudit -> {
                OperationAuditResponseSearch operationAuditResponseSearch = new OperationAuditResponseSearch();
                BeanUtils.copyProperties(operationAudit, operationAuditResponseSearch);
                operationAuditResponseSearch.setOperationTime(DateUtils.convertLongToStringFormatDateTime(
                        operationAudit.getOperationTime(), DateTimeEnum.dd_MM_yyy_HHmm));

                return operationAuditResponseSearch;
            }).collect(Collectors.toList());

        }

        Long total = operationAudits.getTotalElements();

        OperationAuditResponsePagination operationAuditResponsePagination = new OperationAuditResponsePagination();
        operationAuditResponsePagination.setOperationAuditResponseSearches(operationAuditResponseSearches);
        operationAuditResponsePagination.setTotal(total);

        return Result.ok(operationAuditResponsePagination);
    }

    private OperationAuditResponse getOperationAuditResponse(OperationAudit operationAudit) {
        OperationAuditResponse operationAuditResponse = new OperationAuditResponse();
        try {
            BeanUtils.copyProperties(operationAudit, operationAuditResponse);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return operationAuditResponse;
    }
}
