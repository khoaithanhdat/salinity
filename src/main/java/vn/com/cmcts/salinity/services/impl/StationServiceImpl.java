package vn.com.cmcts.salinity.services.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.com.cmcts.common.exception.DataNotFoundException;
import vn.com.cmcts.salinity.model.Station;
import vn.com.cmcts.salinity.model.payload.request.StationRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.StationRequestUpdate;
import vn.com.cmcts.salinity.model.payload.response.Result;
import vn.com.cmcts.salinity.model.payload.response.StationResponse;
import vn.com.cmcts.salinity.model.payload.response.StationResponsePagination;
import vn.com.cmcts.salinity.repository.StationRepository;
import vn.com.cmcts.salinity.services.StationService;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 15:54
 * @updated_by dat
 * @updated_date 06/01/2022 15:54
 * @since 06/01/2022 15:54
 */

@Service
public class StationServiceImpl implements StationService {

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public Result<StationResponse> create(StationRequestCreate stationRequestCreate) throws Exception {
        Station station = new Station();
        BeanUtils.copyProperties(stationRequestCreate, station);
        station.setId(UUID.randomUUID().toString());

        stationRepository.save(station);

        StationResponse stationResponse = Optional.ofNullable(station).map(this::getStationResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("station.create.error",null,null)));

        return Result.ok(stationResponse);
    }

    @Override
    public Result<StationResponse> update(StationRequestUpdate stationRequestUpdate, String id) throws Exception {
        Station station = stationRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(messageSource.getMessage("station.not.found",null,null)));
        stationRequestUpdate.setInput(station.getInput());
        stationRequestUpdate.setOutput(station.getOutput());
        BeanUtils.copyProperties(stationRequestUpdate, station);

        stationRepository.save(station);

        StationResponse stationResponse = Optional.ofNullable(station).map(this::getStationResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("station.update.error",null,null)));

        return Result.ok(stationResponse);
    }

    @Override
    public Result<StationResponse> delete(String id) throws Exception {
        Station station = stationRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(messageSource.getMessage("station.not.found",null,null)));

        stationRepository.delete(station);

        StationResponse stationResponse = Optional.ofNullable(station).map(this::getStationResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("station.delete.error",null,null)));

        return Result.ok(stationResponse);
    }

    @Override
    public Result<StationResponsePagination> getAll(Pageable pageable) {
        StationResponsePagination stationResponsePagination = new StationResponsePagination();
        Page<StationResponse> stationResponses = stationRepository.getAll(pageable);

        long total = stationResponses.getTotalElements();
        stationResponsePagination.setTotal(total);

        if (!CollectionUtils.isEmpty(stationResponses.getContent()))
            stationResponsePagination.setStationResponses(stationResponses.getContent());
        else
            stationResponsePagination.setStationResponses(new ArrayList<>());

        return Result.ok(stationResponsePagination);
    }

    private StationResponse getStationResponse(Station station) {
        StationResponse stationResponse = new StationResponse();
        try {
            BeanUtils.copyProperties(station, stationResponse);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stationResponse;
    }
}
