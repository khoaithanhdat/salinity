package vn.com.cmcts.salinity.services;

import org.springframework.data.domain.Pageable;
import vn.com.cmcts.salinity.model.payload.request.OperationAuditRequest;
import vn.com.cmcts.salinity.model.payload.request.OperationAuditRequestSearch;
import vn.com.cmcts.salinity.model.payload.response.OperationAuditResponse;
import vn.com.cmcts.salinity.model.payload.response.OperationAuditResponsePagination;
import vn.com.cmcts.salinity.model.payload.response.Result;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 17:48
 * @updated_by dat
 * @updated_date 06/01/2022 17:48
 * @since 06/01/2022 17:48
 */
public interface OperationAuditService {

    Result<OperationAuditResponse> create(OperationAuditRequest operationAuditRequest) throws Exception;

    Result<OperationAuditResponsePagination> getByCondition(OperationAuditRequestSearch operationAuditRequestSearch, Pageable pageable);
}
