package vn.com.cmcts.salinity.services;

import org.springframework.data.domain.Pageable;
import vn.com.cmcts.salinity.model.payload.request.SettingRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.SettingRequestUpdate;
import vn.com.cmcts.salinity.model.payload.response.Result;
import vn.com.cmcts.salinity.model.payload.response.SettingResponse;
import vn.com.cmcts.salinity.model.payload.response.SettingResponsePagination;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 16:38
 * @updated_by dat
 * @updated_date 06/01/2022 16:38
 * @since 06/01/2022 16:38
 */
public interface SettingService {

    Result<SettingResponse> update(SettingRequestUpdate settingRequestUpdate) throws Exception;

    Result<SettingResponse> delete(String id) throws Exception;

    Result<SettingResponsePagination> getAll(Pageable pageable);

    Result<String> getByName(String name);
}
