package vn.com.cmcts.salinity.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import vn.com.cmcts.common.exception.DataNotFoundException;
import vn.com.cmcts.salinity.model.Import;
import vn.com.cmcts.salinity.model.payload.request.ImportRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.ImportRequestUpdate;
import vn.com.cmcts.salinity.model.payload.request.ImportRequestUploadFile;
import vn.com.cmcts.salinity.model.payload.response.*;
import vn.com.cmcts.salinity.repository.ImportRepository;
import vn.com.cmcts.salinity.services.ImportService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ImportServiceImpl implements ImportService {
    @Autowired
    ImportRepository importRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public ResponseEntity<Result<String>> importFile(MultipartFile file, String ImportRequestUploadFileString) throws IOException {

        ImportRequestUploadFile importRequest = new ImportRequestUploadFile();
        ObjectMapper objectMapper = new ObjectMapper();
        importRequest = objectMapper.readValue(ImportRequestUploadFileString, ImportRequestUploadFile.class);

        //Check mã trạm
        if(importRepository.checkStationCode(importRequest.getStation()) == 0 ){
            return ResponseEntity.ok().body(Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), messageSource.getMessage("station.not.found",null,null)));
        }

        Boolean isCheckYear = (importRequest.getYear_input() == null || importRequest.getYear_input() == 0) ? true : false;
        Boolean isCheckLineInput = (importRequest.getLine_from() == null && importRequest.getLine_to() == null) || (importRequest.getLine_from() == 0 && importRequest.getLine_to() == 0) ? true : false;

        List<Import> tempDataImport = new ArrayList<>();
        XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        if (isCheckLineInput) {
            for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
                if (getDataImport(importRequest, isCheckYear, tempDataImport, worksheet, i)) return ResponseEntity.badRequest()
                        .body(Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), messageSource.getMessage("import.file.create.error.date",null,null)));
            }
        } else {
            for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
                if (i >= importRequest.getLine_from() && i <= importRequest.getLine_to()) {
                    if (getDataImport(importRequest, isCheckYear, tempDataImport, worksheet, i))
                        return ResponseEntity.badRequest()
                                .body(Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), messageSource.getMessage("import.file.create.error.date",null,null)));
                }
            }
        }

        //Ghi đè data cũ
        if (!importRequest.isOverwrite()) {
            List<Import> importList = importRepository.findDataOverWrite(importRequest.getStation(), importRequest.getYear_input());
            importRepository.deleteAll(importList);
        }

        importRepository.saveAll(tempDataImport);

        return ResponseEntity.ok()
                .body(Result.ok(messageSource.getMessage("import.file.create.successfully",null,null)));
    }

    @Override
    public Result<ImportResponsePage> getByCondition(Pageable pageable) {
        List<ImportResponseSearch> importResponseList = null;

        Page<ImportResponseSearch> importPage = importRepository.findDataByCondition(pageable);

        if (!CollectionUtils.isEmpty(importPage.getContent())) {
            importResponseList = importPage.getContent().stream().map(item -> {
                ImportResponseSearch importResponse = new ImportResponseSearch();
                BeanUtils.copyProperties(item, importResponse);
                return importResponse;
            }).collect(Collectors.toList());

        }

        Long total = importPage.getTotalElements();

        ImportResponsePage importResponsePage = new ImportResponsePage();
        importResponsePage.setImportResponseList(importResponseList);
        importResponsePage.setTotal(total);

        return Result.ok(importResponsePage);
    }

    @Override
    public Result<ImportResponse> create(ImportRequestCreate importRequestCreate) throws Exception {
        Import importData= new Import();
        BeanUtils.copyProperties(importRequestCreate, importData);
        importData.setId(UUID.randomUUID().toString());

        importRepository.save(importData);

        ImportResponse importResponse = Optional.ofNullable(importData).map(this::getImportResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("import.create.error",null,null)));

        return Result.ok(importResponse);
    }

    @Override
    public Result<ImportResponse> updateById(String id, ImportRequestUpdate importRequestUpdate) throws Exception {
        Import importData = importRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(messageSource.getMessage("import.not.found",null,null)));

        BeanUtils.copyProperties(importRequestUpdate, importData);

        importRepository.save(importData);

        ImportResponse stationResponse = Optional.ofNullable(importData).map(this::getImportResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("import.update.error",null,null)));

        return Result.ok(stationResponse);
    }

    @Override
    public Result<ImportResponse> deleteById(String id) throws Exception {
        Import importData = importRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(messageSource.getMessage("import.not.found",null,null)));

        importRepository.delete(importData);

        ImportResponse stationResponse = Optional.ofNullable(importData).map(this::getImportResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("import.delete.error",null,null)));

        return Result.ok(stationResponse);
    }

    private @NotNull
    Import readDataRow(XSSFRow row, ImportRequestUploadFile importRequestUploadFile) {
        Import dataImport = new Import();

        dataImport.setId(UUID.randomUUID().toString());
        dataImport.setNo(Math.round(row.getCell(0).getNumericCellValue()));
        dataImport.setDay(row.getCell(1).getDateCellValue());
        dataImport.setTime((int) row.getCell(2).getNumericCellValue());
        dataImport.setSalinity(row.getCell(3).getNumericCellValue());
        dataImport.setStation(importRequestUploadFile.getStation());

        return dataImport;
    }

    private boolean getDataImport(ImportRequestUploadFile importRequestUploadFile, Boolean isCheckYear, List<Import> tempDataImport, XSSFSheet worksheet, int i) {
        if (isCheckYear) {
            tempDataImport.add(readDataRow(worksheet.getRow(i), importRequestUploadFile));
        } else {
            int year = worksheet.getRow(i).getCell(1).getDateCellValue().getYear() + 1900;
            if (year == importRequestUploadFile.getYear_input()) {
                tempDataImport.add(readDataRow(worksheet.getRow(i), importRequestUploadFile));
            } else
                return true;
        }
        return false;
    }

    private ImportResponse getImportResponse(Import importData) {
        ImportResponse importResponse = new ImportResponse();
        try {
            BeanUtils.copyProperties(importData, importResponse);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return importResponse;
    }
}
