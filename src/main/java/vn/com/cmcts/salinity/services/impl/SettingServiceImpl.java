package vn.com.cmcts.salinity.services.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.com.cmcts.common.exception.DataNotFoundException;
import vn.com.cmcts.salinity.model.Setting;
import vn.com.cmcts.salinity.model.payload.request.SettingRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.SettingRequestUpdate;
import vn.com.cmcts.salinity.model.payload.response.Result;
import vn.com.cmcts.salinity.model.payload.response.SettingResponse;
import vn.com.cmcts.salinity.model.payload.response.SettingResponsePagination;
import vn.com.cmcts.salinity.repository.SettingRepository;
import vn.com.cmcts.salinity.services.SettingService;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 16:38
 * @updated_by dat
 * @updated_date 06/01/2022 16:38
 * @since 06/01/2022 16:38
 */

@Service
public class SettingServiceImpl implements SettingService {

    @Autowired
    private SettingRepository settingRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public Result<SettingResponse> update(SettingRequestUpdate settingRequestUpdate) throws Exception {
        Setting setting = null;
        setting = settingRepository.findByName(settingRequestUpdate.getName());
        if (setting == null) {
            setting = new Setting();
            setting.setId(UUID.randomUUID().toString());
        }
        BeanUtils.copyProperties(settingRequestUpdate, setting);

        settingRepository.save(setting);

        SettingResponse settingResponse = Optional.ofNullable(setting).map(this::getSettingResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("setting.update.error", null, null)));

        return Result.ok(settingResponse);
    }

    @Override
    public Result<SettingResponse> delete(String id) throws Exception {
        Setting setting = settingRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(messageSource.getMessage("setting.not.found", null, null)));

        settingRepository.delete(setting);

        SettingResponse settingResponse = Optional.ofNullable(setting).map(this::getSettingResponse)
                .orElseThrow(() -> new Exception(messageSource.getMessage("setting.delete.error", null, null)));

        return Result.ok(settingResponse);
    }

    @Override
    public Result<SettingResponsePagination> getAll(Pageable pageable) {
        SettingResponsePagination settingResponsePagination = new SettingResponsePagination();
        Page<SettingResponse> settingResponses = settingRepository.getAll(pageable);
        if (!CollectionUtils.isEmpty(settingResponses.getContent()))
            settingResponsePagination.setSettingResponses(settingResponses.getContent());
        else
            settingResponsePagination.setSettingResponses(new ArrayList<>());

        long total = settingResponses.getTotalElements();
        settingResponsePagination.setTotal(total);

        return Result.ok(settingResponsePagination);
    }

    @Override
    public Result<String> getByName(String name) {
        String value = settingRepository.getValueByName(name);

        return Result.ok(value);
    }

    private SettingResponse getSettingResponse(Setting setting) {
        SettingResponse settingResponse = new SettingResponse();
        try {
            BeanUtils.copyProperties(setting, settingResponse);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return settingResponse;
    }
}
