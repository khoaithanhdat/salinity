package vn.com.cmcts.salinity.services;

import org.springframework.data.domain.Pageable;
import vn.com.cmcts.salinity.model.payload.request.StationRequestCreate;
import vn.com.cmcts.salinity.model.payload.request.StationRequestUpdate;
import vn.com.cmcts.salinity.model.payload.response.Result;
import vn.com.cmcts.salinity.model.payload.response.StationResponse;
import vn.com.cmcts.salinity.model.payload.response.StationResponsePagination;

/**
 * https://cmcts.com.vn/.
 *
 * @author dat
 * @version 1.0
 * @created_by dat
 * @created_date 06/01/2022 15:53
 * @updated_by dat
 * @updated_date 06/01/2022 15:53
 * @since 06/01/2022 15:53
 */
public interface StationService {

    Result<StationResponse> create(StationRequestCreate stationRequestCreate) throws Exception;

    Result<StationResponse> update(StationRequestUpdate stationRequestUpdate,String id) throws Exception;

    Result<StationResponse> delete(String id) throws Exception;

    Result<StationResponsePagination> getAll(Pageable pageable);
}
